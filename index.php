<?php
//does $_GET['page'] exist?
//print($_GET['page']);
$_IS_LOADED=true;
if(!array_key_exists('page',$_GET))
{
    //if no page is specified, use the default: landing.php
    $page = 'landing.php';
}
else
{
    //if a page is specified, use that
    $page = $_GET['page'].'.php';
}
$page='pages/'.$page;

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    
    <meta name="title" content="Home of the Sweet Corner" />
    <meta name="keywords" content="sweetcorner, sweet, corner, home, index" />
    <meta name="description" content="This is the home of the Sweeet Corner." />
    
    <title>Sweet Corner | Home</title>
    <link media="all" type="text/css" rel="stylesheet" href="main.css" />
    
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script'
          rel='stylesheet' type='type/css' />
</head>
<body>
<?php
    include 'includes/header.php';
?>
<!------------------------------------------------------------------------------>
    <main>
        <?php
            include $page;
        ?>
    </main>
<!---------------------------------------------------------------------------------->
<?php
    include 'includes/footer.php';
?>
    
</body>
</html>









