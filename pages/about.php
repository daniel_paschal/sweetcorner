<?php
if(!isset($_IS_LOADED) || !$_IS_LOADED)
{
    exit("unauthorized access");
}
?>
<!----------------------------------------------------------------------------------->
    <main id="about-middle">
        <img src="images/dots-box1.png" id="dots-about" /> 
        <section id="middle-about">
            <div id ="middle-text-about">
        <h3>Delicious cupcakes, magical moments!</h3>
            <p>Sweet Corner is growing fast. Our deliscious cupcakes of all sizes, shapes and colours and tastes are creating a real wow factor. </p>
            <p>Our deliscious cupcakes are different from other bakeries. Why? Well, not onl are our cupcakes and baked goods prepared fresh every day, we also create personalised cupcakes in any size and shape just for you! Simply Delicious and unique.</p>
            <p>Our expert bakers create stunning cupcakes in any flavour, colour, size or shape you choose.</p>
            <p>Have you seen a stunning cupcake in a magazine, a TV show or on the internet? Want something unique to reflect a character or personality? Sweet Corner's cupcakes are baked with love and that's why we can create perfect and unique cupcake for you</p>
            <p>No idea is too creative for our bakers at Sweet Corner cupcakes. Contact us today to discuss your special cupcake. </p>
            </div>
          
        </section>
    </main>
    <div id="box-1-about">
    <img src="images/box1.png"/> 
    </div>
    
    
    
    