    <header id="main-header">
        <img src="images/header.png" id="header-img" />
        <ul class="list-organize">
            <li><a href="?page=landing">Home</a></li>
            <li><a href="?page=about">About Us</a></li>
            <li><a href="?page=services">Services</a></li>
            <li><a href ="contact.html">Contact</a></li>
        </ul>
        <div id="main-logo"></div>
        <h3 class="dancin" id="text-below-logo">We deliver cupcakes for the important events in your life!</h3>
        <div id="brown-line"></div>
    </header>